<?php

class ProductController extends Zend_Controller_Action
{

    public function init()
    {
        /* Initialize action controller here */
    }

    public function indexAction()
    {
        // action body
        $this->redirect("Product/list");
    }

    public function addAction()
    {
        // action body
        $form = new Application_Form_Product();

        $category_model = new Application_Model_Category();
        $categories = $category_model->listCategories();

        if (!empty($categories)) {
            for ($i = 0; $i < count($categories); $i++) {
                $form->getElement('cat_id')->addMultiOption($categories[$i]["id"], $categories[$i]["name"]);
            }
        }

        $this->view->form = $form;

        if ($this->getRequest()->isPost()) {
            if ($form->isValid($this->getRequest()->getParams())) {
                $this->view->data = $form->getValues();
                $product_model = new Application_Model_Products();
                $product_model->addProduct($form->getValues());
                $this->redirect("product/list");
            }
        }
    }

    public function editAction()
    {
        // action body
        $form = new Application_Form_Product();
        $this->view->form = $form;
        $id = $this->getRequest()->getParam("id");
        
        if ($id) {
            $product_model = new Application_Model_Products();
            $product = $product_model->getProductByID($id);
            
            $form->populate($product[0]);
            $this->render("add");
            
            if ($this->getRequest()->isPost()) {
                if ($form->isValid($this->getRequest()->getParams())) {
                    $product_model = new Application_Model_Products();
                    $product_model->updateProduct($form->getValues());
                    $this->redirect("product/list");
                }
            }
        }
    }

    public function listAction()
    {
        // action body
        $prod_model = new Application_Model_Products();
        if (empty($prod_model->listProducts())) {
            $this->view->msg = "No Products to display!";
        }
        $this->view->products = $prod_model->listProducts();
    }

    public function testAction()
    {
        // action body
    }


}

