<?php

class OrdersController extends Zend_Controller_Action {

    public function init() {
        /* Initialize action controller here */
    }

    public function indexAction() {
        // action body
    }

    public function makeOrderAction() {
        $order = new Application_Model_OrderProduct();
        $products = new Application_Model_Products();
        $products = $products->listProducts();
        $this->view->products = $products;
        $userId = "1";
        $latestOrdrs = $order->getLatestOrders($userId);
        $this->view->latestOrders = $latestOrdrs;
        if (($this->getRequest()->getParam("order")) != null) {
            $data = $this->getRequest()->getParams();
            unset($data['controller']);
            unset($data['action']);
            unset($data['module']);
            unset($data['order']);

            $order->OrderProduct($data, "1");
            $this->redirect("orders/make-order");
        }
    }

    public function listOrdersAction() {
        $room = new Application_Model_Rooms();
        $user = new Application_Model_Users();
        $order = new Application_Model_Orders();
        $product = new Application_Model_Products();
        $ext = new Application_Model_Extensions();
        $ordProd = new Application_Model_OrderProduct();
        $data = array();
        $orders = $order->listOrders();
        $counter = 0;
        foreach ($orders as $ord) {
            $data[$counter]["name"] = $user->getUser($ord["u_id"])["name"];
            $data[$counter]["date"] = $ord["date"];
            $data[$counter]["room"] = $room->getRoom($user->getUser($ord["u_id"])["room_no"]);
            $data[$counter]["ext"] = $ext->getExt($user->getUser($ord["u_id"])["ext_no"]);
            $data[$counter]["action"] = $ord["action"];
            $data[$counter]["product"] = array();
            $products = $ordProd->getOrderProducts($ord["id"]);
            $pcounter = 0;
            $total = 0;

            foreach ($products as $prod) {

                $data[$counter]["product"][$pcounter]["name"] = $product->getProduct($prod['prod_id'])["name"];
                $qty = $data[$counter]["product"][$pcounter]["qty"] = $prod["quantity"];
                $price = $data[$counter]["product"][$pcounter]["price"] = $product->getProduct($prod['prod_id'])["price"];
                $total+=$price * $qty;
                $pcounter++;
            }
            $data[$counter]["total"] = $total;
            $pcounter = 0;
            $total = 0;
            $counter++;
        }
        $counter = 0;

        $this->view->data = $data;
    }

    public function listUserOrdersAction() {
        $order = new Application_Model_Orders();
        $product = new Application_Model_Products();
        $ordProd = new Application_Model_OrderProduct();
        $data = array();
        if (($this->getRequest()->getParam("find")) != null) {
            $date1 = $this->getRequest()->getParam("date1");
            $date2 = $this->getRequest()->getParam("date2");
            if (empty($date1) | empty($date2)) {

                $this->view->error = "insert date";
            } else {
                $orders = $order->listUserOrdersByDate("1", $date1, $date2);
            }
        } else {

            $orders = $order->listUserOrders("1");
        }
        $counter = 0;
        foreach ($orders as $ord) {
            $data[$counter]["date"] = $ord["date"];
            $data[$counter]["action"] = $ord["action"];
            $data[$counter]["product"] = array();
            $products = $ordProd->getOrderProducts($ord["id"]);
            $pcounter = 0;
            $total = 0;

            foreach ($products as $prod) {

                $data[$counter]["product"][$pcounter]["name"] = $product->getProduct($prod['prod_id'])["name"];
                $qty = $data[$counter]["product"][$pcounter]["qty"] = $prod["quantity"];
                $price = $data[$counter]["product"][$pcounter]["price"] = $product->getProduct($prod['prod_id'])["price"];
                $total+=$price * $qty;
                $pcounter++;
            }
            $data[$counter]["total"] = $total;
            $pcounter = 0;
            $total = 0;
            $counter++;
        }
        $counter = 0;

        $this->view->data = $data;
    }

}
