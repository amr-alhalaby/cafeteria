<?php
class Application_Form_Product extends Zend_Form
{
    public function init()
    {
        /* Form Elements & Other Definitions Here ... */
        $this->setMethod('post');
        
        $pname = new Zend_Form_Element_Text("name");
        $pname->setRequired();
        $pname->setLabel("Category Name: ");
        $pname->addValidator(new Zend_Validate_Alpha());
        $pname->addErrorMessage('Please enter product name in characters');
        
        $price = new Zend_Form_Element_Text("price");
        $price->setRequired();
        $price->setLabel("Price:");
        $price->addValidator(new Zend_Validate_Digits());
        
        $category = new Zend_Form_Element_Select("categories");
        $category->setRequired();
        $category->setLabel("Category: ");
        
        $pic = new Zend_Form_Element_File("Product Picture");
        $pic->addValidator(new Zend_Validate_File_IsImage());
        $pic->addErrorMessage('not an image');
        $pic->setLabel("Poduct Picture:");
        
        $save = new Zend_Form_Element_Submit("submit");
        $reset = new Zend_Form_Element_Reset("reset");
        
        $this->addElements(array($pname,$price,$category,$pic,$save,$reset));
    }
}