<?php
class Application_Form_Product extends Zend_Form
{
    public function init()
    {
        /* Form Elements & Other Definitions Here ... */
        $this->setMethod('post');
        $this->setEnctype('multipart/form-data');
        
        $pname = new Zend_Form_Element_Text("name");
        $pname->setRequired();
        $pname->setLabel("Product Name: ");
        $pname->addValidator(new Zend_Validate_Alpha());
        $pname->addErrorMessage('product name must be in characters only');
        
        $price = new Zend_Form_Element_Text("price");
        $price->setRequired();
        $price->setLabel("Price:");
        $price->addValidator(new Zend_Validate_Digits());
        $pname->addErrorMessage('product price must be in digits only');
        
        $category = new Zend_Form_Element_Select("cat_id");
        $category->setRequired();
        $category->setLabel("Category: ");
        
        $pic = new Zend_Form_Element_File("image");
        $pic->addValidator(new Zend_Validate_File_IsImage());
        $pic->addErrorMessage('not an image');
        $pic->setRequired();
        $pic->setLabel("Poduct Picture:");
        
        $submit = new Zend_Form_Element_Submit("Submit");
        $reset = new Zend_Form_Element_Reset("Reset");
        
        $this->addElements(array($pname,$price,$category,$pic,$submit,$reset));
    }
}