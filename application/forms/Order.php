<?php

class Application_Form_Order extends Zend_Form {

    public function init() {
        /* Form Elements & Other Definitions Here ... */
        $this->setMethod('post');
        $this->setAttrib("id", "Cart");
        $submit = new Zend_Form_Element_Submit("order");
        $this->addElements(array($tea, $cofe, $submit));
    }

}
