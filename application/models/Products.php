<?php

class Application_Model_Products extends Zend_Db_Table_Abstract {

    protected $_name = 'products';

    function getProductId($productName) {

        $row = $this->fetchAll($this->select()->where("name='$productName'"))->toArray();
        return $row[0]['id'];
    }

    function getProduct($productId) {

        $row = $this->fetchAll($this->select()->where("id=$productId"))->toArray();
        return $row[0];
    }

    function addProduct($data) {
        return $this->insert($data);
    }

    function updateProduct($data) {
        return $this->update($data, "id=" . $data['id']);
    }

    function getProductByID($id) {
        return $this->find($id)->toArray();
    }

    function listProducts() {
        return $this->fetchAll()->toArray();
    }

}
