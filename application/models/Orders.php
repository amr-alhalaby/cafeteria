<?php

class Application_Model_Orders extends Zend_Db_Table_Abstract {

    protected $_name = 'orders';

    function addOrder($u_id, $note) {
        $data['u_id'] = $u_id;
        $data['note'] = $note;
        return $this->insert($data);
    }

    function getOrder($orderId) {
        $rows = $this->fetchAll(
                        $this->select()
                                ->where("id=$orderId"))->toArray();

        return $rows[0];
    }

    function listOrders() {

        $rows = $this->fetchAll()->toArray();
        return $rows;
    }

    function listUserOrders($userId) {


        $rows = $this->fetchAll($this->select()->where("u_id=$userId"))->toArray();
        return $rows;
    }

    function listUserOrdersByDate($userId, $date1, $date2) {
        $rows = $this->fetchAll($this->select()->where("u_id=$userId and date between '$date1' and '$date2'"))->toArray();
        return $rows;
    }

}
