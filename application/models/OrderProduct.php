<?php

class Application_Model_OrderProduct extends Zend_Db_Table_Abstract {

    protected $_name = 'order_product';

    function OrderProduct($data, $userId) {
        $order = new Application_Model_Orders();
        $product = new Application_Model_Products();

        if (!empty($data)) {

            $orderId = $order->addOrder($userId);

            if (!empty($orderId)) {
                foreach ($data as $key => $value) {
                    $productId = $product->getProductId($key);

                    $this->add($value, $orderId, $productId);
                }
            }
        }
    }

    function add($quantity, $ord_id, $prod_id) {
        $data['order_id'] = $ord_id;
        $data['prod_id'] = $prod_id;
        $data['quantity'] = $quantity;
        $this->insert($data);
    }

    function getOrderProducts($orderId) {

        $rows = $this->fetchAll(
                        $this->select()
                                ->where("order_id=$orderId"))->toArray();

        return $rows;
    }

    function getLatestOrders($userId) {
        $db = Zend_Db::factory('Pdo_Mysql', array(
                    'host' => '127.0.0.1',
                    'username' => 'root',
                    'password' => '',
                    'dbname' => 'cafeteria'
        ));
        $stmt = $db->query("SELECT products.name, products.price, orders.date,products.image
                            FROM order_product, orders, products,users
                            WHERE order_product.order_id = orders.id
                            AND order_product.prod_id = products.id
                            and users.id=1
                            ORDER BY DATE DESC 
                            LIMIT 4
                            ");
        $rows = $stmt->fetchAll();
        return $rows;
    }

}
