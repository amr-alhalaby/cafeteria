<?php

class Application_Model_Category extends Zend_Db_Table_Abstract
{
    protected $_name = "categories";

    function addCategory($data) {
        return $this->insert($data);
    }

    function updateCategory($data) {
        return $this->update($data, "id=" . $data['id']);
    }

    function getCategoryByID($id) {
        return $this->find($id)->toArray();
    }

    function listCategories() {
        return $this->fetchAll()->toArray();
    }
    
    function listCategoryNames() {
        return $this->fetchAll("name")->toArray();
    }

}

