<?php

class Application_Model_Extensions extends Zend_Db_Table_Abstract{

    protected $_name = 'extensions';

    function getExt($extId) {

        $rows = $this->fetchAll(
                        $this->select()
                                ->where("id=$extId"))->toArray();

        return $rows[0]["extension"];
    }

}
