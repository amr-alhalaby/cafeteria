var addToCartBtns = document.getElementsByClassName("addToCartbtn");

function product(name,price){

	this.name = name;
	this.price = price;
	this.quantity=1;
}

function ShoopingCart(){

	this.arrayOfProducts=[];
	this.items=[];

	this.updateProductInfoGUI=function(itemIndex){

		//update quantity
		document.getElementById("product-"+this.items[itemIndex].name).getElementsByTagName("td")[1].getElementsByTagName("div")[0].getElementsByTagName("input")[0].value=this.items[itemIndex].quantity;

		//update total price
		var itemPrice=shoppingCart.items[itemIndex].price.substr(0,shoppingCart.items[itemIndex].price.indexOf("EGP"));
		document.getElementById("product-"+this.items[itemIndex].name).getElementsByTagName("td")[2].getElementsByTagName("span")[0].childNodes[0].nodeValue=itemPrice*this.items[itemIndex].quantity+"EGP";

	};


	this.getNumOfItems = function(){

		var counter=0;

		for(var i=0;i<this.items.length;i++){

			if(this.items[i] != "removed item")
				counter ++;
		}

		return counter;
	}

	this.enableMinusBtn=function(itemIndex){

		if(this.items[itemIndex].quantity > 1){

			document.getElementById("minus-"+itemIndex).disabled=false;
		}

		this.updateProductInfoGUI(itemIndex);

	}

	this.addItems=function(productName , productPrice,productIndex){


		if(productName == null && productPrice == null && productIndex!=null){

			var itemIndex=parseInt(productIndex);
			this.items[itemIndex].quantity++;

			this.enableMinusBtn(itemIndex);

			this.updateProductInfoGUI(itemIndex);

		}else{

			//checks if the product alreday exists in the shopping cart if yes update quantity else add new item
			if(!this.is_exists(productName)){

				this.items.push(new product(productName,productPrice));

				if(this.getNumOfItems() == 1){

					var cart = document.getElementById("Cart");
					var EmptyCartText = cart.childNodes[0];

					EmptyCartText.parentNode.removeChild(EmptyCartText);
					document.getElementById("cartTable").style.visibility="visible";
				}

			}else{

				var itemIndex=this.getProductIndex(productName);
				this.items[itemIndex].quantity++;
				this.enableMinusBtn(itemIndex);

				
			}
		}

	};

	this.minusItem=function(itemIndex){

		var itemIndex=parseInt(itemIndex);

		this.items[itemIndex].quantity--;
		if(this.items[itemIndex].quantity == 1)
		{
			document.getElementById("minus-"+itemIndex).disabled=true;
		}

		
		this.updateProductInfoGUI(itemIndex);
	};

	this.removeItem=function(itemIndex){

		var itemIndex=parseInt(itemIndex);

		var itemName = this.items[itemIndex].name;
		var itemRow = document.getElementById("product-"+itemName);
		itemRow.parentNode.removeChild(itemRow);

		this.items[itemIndex]="removed item";

		if(this.getNumOfItems() == 0){

			this.items=[];
			var cart = document.getElementById("Cart");
			cart.insertBefore(document.createTextNode("The cart is empty"),cart.firstChild);

			document.getElementById("cartTable").style.visibility="hidden";

		}

	}

	this.getProductIndex = function(productName){

		for(var i=0;i<this.items.length;i++){

			if(this.items[i].name == productName){
				return i;
			}
		}
	};

	this.getProductByIndex=function(i){

		return this.items[i];
	}

	this.is_exists=function(productName){

		//this function returns true if the product exists before in the cart and returns false if it exists

		var exists = false;
		for(var i=0;i<this.items.length;i++){

			if(productName == this.items[i].name){

				exists =true;
				break;
			}
				
		}
		return exists;
	}
}

var shoppingCart = new ShoopingCart();
document.getElementById("cartTable").style.visibility="hidden";

for(var i=0;i<addToCartBtns.length;i++){

	var handleAddingProductToCartGUI = function(addBtn){

		var productAddBtn = document.getElementById(addBtn.id);

		var cart = document.getElementById("cartTable");
		//add a product to cart
		var productAddedRow = document.createElement("tr");
		productAddedRow.id = "product-"+productAddBtn.id;//product-productName
		cart.appendChild(productAddedRow);

		var productName = document.createElement("td");
		var productNameString = productAddBtn.id;
		productName.appendChild(document.createTextNode(productNameString));
		productAddedRow.appendChild(productName);


		var secondCol = document.createElement("td");
		productAddedRow.appendChild(secondCol);

		var productAddDeleteDiv = document.createElement("div");
		productAddDeleteDiv.className = "input-append";
		secondCol.appendChild(productAddDeleteDiv);

		var productValue = document.createElement("input");
		productValue.className = "span3 text-center";
		productValue.type = "text";
		productValue.style.cssText="width:30px;height:30px;";
		productValue.id="quantity";
		productValue.value=1;
		productAddDeleteDiv.appendChild(productValue);


		var addBtn = document.createElement("button");
		addBtn.id=shoppingCart.items.length;
		addBtn.onclick=function(){

			shoppingCart.addItems(null,null,this.id);

		}
		addBtn.className = "btn btn-success";
		addBtn.appendChild(document.createTextNode("+"));
		productAddDeleteDiv.appendChild(addBtn);


		var removeBtn = document.createElement("button");
		removeBtn.className = "btn btn-inverse";
		removeBtn.id="minus-"+shoppingCart.items.length;
		removeBtn.disabled=true;
		removeBtn.onclick=function(){

			var itemIndex = this.id.split("-")[1];
			shoppingCart.minusItem(itemIndex);
		}
		removeBtn.appendChild(document.createTextNode("-"));
		productAddDeleteDiv.appendChild(removeBtn);

		var totalPrice = document.createElement("td");
		totalPrice.className="tdRight";
		productAddedRow.appendChild(totalPrice);
		var productPrice = productAddBtn.parentNode.parentNode.getElementsByTagName("span")[0].childNodes[0].nodeValue;

		var totalPriceSpan = document.createElement("span");
		totalPriceSpan.appendChild(document.createTextNode(productPrice));
		totalPrice.appendChild(totalPriceSpan);

		var removeProduct = document.createElement("td");
		removeProduct.className = "tdCenter";
		productAddedRow.appendChild(removeProduct);

		var removeLink = document.createElement("a");
		removeLink.id="remove-"+shoppingCart.items.length;
		removeLink.className="glyphicon glyphicon-remove-sign";
		removeLink.appendChild(document.createTextNode(""));
		removeLink.onclick=function(){

			var itemIndex=this.id.split("-")[1];
			shoppingCart.removeItem(itemIndex);
		}
		// removeLink.href="";
		removeProduct.appendChild(removeLink);

	}

	var AddProductToArray = function(product){

		var product = document.getElementById(product.id);
		var productName = product.id;
		var productPrice = product.parentNode.parentNode.getElementsByTagName("span")[0].childNodes[0].nodeValue;

		shoppingCart.addItems(productName,productPrice);
	}

	addToCartBtns[i].onclick=function(){

		var addBtn = this;
		if(!shoppingCart.is_exists(addBtn.id)){

			handleAddingProductToCartGUI(addBtn);	
		}
		
		AddProductToArray(addBtn);

		//update quantity of the product in cart
		var itemIndex = shoppingCart.getProductIndex(addBtn.id);
		var itemQt=shoppingCart.items[itemIndex].quantity;
		var itemPrice=shoppingCart.items[itemIndex].price.substr(0,shoppingCart.items[itemIndex].price.indexOf("EGP"));

		//update product quantity
		document.getElementById("product-"+addBtn.id).getElementsByTagName("td")[1].getElementsByTagName("div")[0].getElementsByTagName("input")[0].value=itemQt;

		//update total price
		document.getElementById("product-"+addBtn.id).getElementsByTagName("td")[2].getElementsByTagName("span")[0].childNodes[0].nodeValue=itemPrice*itemQt+"EGP";

	}
}